import random
from django.conf import settings
from .models import Card, ROYALS, SUITS

def create_deck():
    cardfaces, deck = [], []
    for i in range(2, 11):
        cardfaces.append(str(i))
    
    for j in range(4):
        cardfaces.append(ROYALS[j])

    for k in range(4):
        for l in range(13):
            card = (cardfaces[l] + ' ' + SUITS[k])
            deck.append(card)
    
    for d in deck:
        card = Card(
            card = d
        )
        card.save()