from django.db import models
#from .utils import create_deck

SUITS = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
ROYALS = ['J', 'Q', 'K', 'A']

class Card(models.Model):
    card = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.card