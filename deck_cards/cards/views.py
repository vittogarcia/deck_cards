import random
from django.http import response
from django.shortcuts import render, get_list_or_404
from .models import Card

def home_view(request):
    template = 'home.html'
    context = {}
    cards = []

    if request.method == 'GET':
        return render(request, template, context)
    elif request.method == 'POST':
        try:
            if 'show' in request.POST:
                context['cards'] = Card.objects.all()
                print(context['cards'])
            elif 'shuffle' in request.POST:
                for s in Card.objects.all().values_list('card', flat=True):
                    cards.append(s)
                random.shuffle(cards)
                context['shuffle_cards'] = cards
                print(context['shuffle_cards'])
            elif 'deal' in request.POST:
                deal = []
                for s in Card.objects.all().values_list('card', flat=True):
                    cards.append(s)
                random.shuffle(cards)
                deal = random.sample(cards, 5)
                difference = list(set(cards) ^ set(deal))

                print(deal, difference)
                context['deal_cards'] = deal
                context['rest_cards'] = difference
            
            return render(request, template, context)
        except:
            return render(request, '404.html', status=404)
    else:
        return render(request, '404.html', status=404)


